#!/usr/bin/env python2

from vtk import *
from sys import argv, exit

if len(argv) < 2:
    print("Usage: " + argv[0] + " file.vtu")
    exit(1)

file = argv[1]
print("Reading from", file)
print("Writing to", file)

r = vtkXMLUnstructuredGridReader()
r.SetFileName(file)
r.Update()

m = r.GetOutput()

w = vtkXMLUnstructuredGridWriter()
w.SetFileName(file)
w.SetInputData(m)
w.SetDataModeToAppended()
w.SetCompressorTypeToZLib()
w.Update()
