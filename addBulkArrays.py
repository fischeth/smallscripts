#!/usr/bin/env python3

from vtk import *
from sys import argv, exit

if len(argv) < 3:
    print("Usage: " + argv[0] + " input.vtu output.vtu")
    exit(1)

input_file = argv[1]
output_file = argv[2]
print("Reading from", input_file)
print("Writing to", output_file)

r = vtkXMLUnstructuredGridReader()
r.SetFileName(input_file)
r.Update()

m = r.GetOutput()

point_data = m.GetPointData()
bulk_node_ids = vtkIdTypeArray()
bulk_node_ids.SetNumberOfComponents(1)
bulk_node_ids.SetNumberOfTuples(m.GetNumberOfPoints())
bulk_node_ids.SetName('bulk_node_ids')
for i in range(0, m.GetNumberOfPoints()):
    bulk_node_ids.SetValue(i, i)
point_data.AddArray(bulk_node_ids)

cell_data = m.GetCellData()
bulk_element_ids = vtkIdTypeArray()
bulk_element_ids.SetNumberOfComponents(1)
bulk_element_ids.SetNumberOfTuples(m.GetNumberOfCells())
bulk_element_ids.SetName('bulk_element_ids')
for i in range(0, m.GetNumberOfCells()):
    bulk_element_ids.SetValue(i, i)
cell_data.AddArray(bulk_element_ids)

w = vtkXMLUnstructuredGridWriter()
w.SetFileName(output_file)
w.SetInputData(m)
w.SetDataModeToAscii()
w.SetCompressorTypeToNone()
w.Update()
