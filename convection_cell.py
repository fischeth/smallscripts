import sys
# state file generated using paraview version 5.4.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1102, 783]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [2750.0, -2750.0, -2750.0]
renderView1.StereoType = 0
renderView1.CameraPosition = [-15498.59814557674, -368.79126058597006, -2703.9408266056103]
renderView1.CameraFocalPoint = [2750.000000000006, -2750.0000000000014, -2750.000000000003]
renderView1.CameraViewUp = [0.004325789316856173, 0.013810314262884213, 0.9998952758998048]
renderView1.CameraParallelScale = 4763.139720814413
renderView1.Background = [0.32, 0.34, 0.43]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML Partitioned Unstructured Grid Reader'
mesh = XMLPartitionedUnstructuredGridReader(FileName=[sys.argv[1]])
mesh.CellArrayStatus = ['MaterialIDs']
mesh.PointArrayStatus = ['T', 'darcy_velocity', 'initial_pressure', 'initial_temperature', 'p']

# create a new 'Calculator'
calculator1 = Calculator(Input=mesh)
calculator1.ResultArrayName = 'T-initial_temperature_gradient'
calculator1.Function = 'T-20+150/5500*coordsZ'

# create a new 'Clip'
clip1 = Clip(Input=calculator1)
clip1.ClipType = 'Plane'
clip1.Scalars = ['POINTS', 'T-initial_temperature_gradient']
clip1.Value = -0.09679581942769744

# init the 'Plane' selected for 'ClipType'
clip1.ClipType.Origin = [2750.0, -2750.0, -2750.0]

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'Tinitial_temperature_gradient'
tinitial_temperature_gradientLUT = GetColorTransferFunction('Tinitial_temperature_gradient')
tinitial_temperature_gradientLUT.RGBPoints = [-0.33721017559985, 0.23137254902, 0.298039215686, 0.752941176471, -0.09679581942769744, 0.865, 0.865, 0.865, 0.14361853674445513, 0.705882352941, 0.0156862745098, 0.149019607843]
tinitial_temperature_gradientLUT.NanColor = [0.6431372549019608, 0.0, 0.0]
tinitial_temperature_gradientLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'Tinitial_temperature_gradient'
tinitial_temperature_gradientPWF = GetOpacityTransferFunction('Tinitial_temperature_gradient')
tinitial_temperature_gradientPWF.Points = [-0.33721017559985, 0.0, 0.5, 0.0, 0.14361853674445513, 1.0, 0.5, 0.0]
tinitial_temperature_gradientPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from clip1
clip1Display = Show(clip1, renderView1)
# trace defaults for the display properties.
clip1Display.Representation = 'Surface'
clip1Display.ColorArrayName = ['POINTS', 'T-initial_temperature_gradient']
clip1Display.LookupTable = tinitial_temperature_gradientLUT
clip1Display.OSPRayScaleArray = 'T-initial_temperature_gradient'
clip1Display.OSPRayScaleFunction = 'PiecewiseFunction'
clip1Display.SelectOrientationVectors = 'None'
clip1Display.ScaleFactor = 550.0
clip1Display.SelectScaleArray = 'T-initial_temperature_gradient'
clip1Display.GlyphType = 'Arrow'
clip1Display.GlyphTableIndexArray = 'T-initial_temperature_gradient'
clip1Display.DataAxesGrid = 'GridAxesRepresentation'
clip1Display.PolarAxes = 'PolarAxesRepresentation'
clip1Display.ScalarOpacityFunction = tinitial_temperature_gradientPWF
clip1Display.ScalarOpacityUnitDistance = 216.82507101023216
clip1Display.GaussianRadius = 275.0
clip1Display.SetScaleArray = ['POINTS', 'T-initial_temperature_gradient']
clip1Display.ScaleTransferFunction = 'PiecewiseFunction'
clip1Display.OpacityArray = ['POINTS', 'T-initial_temperature_gradient']
clip1Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
clip1Display.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
clip1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
clip1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# show color legend
clip1Display.SetScalarBarVisibility(renderView1, True)

# setup the color legend parameters for each legend in this view

# get color legend/bar for tinitial_temperature_gradientLUT in view renderView1
tinitial_temperature_gradientLUTColorBar = GetScalarBar(tinitial_temperature_gradientLUT, renderView1)
tinitial_temperature_gradientLUTColorBar.Title = 'T-initial_temperature_gradient'
tinitial_temperature_gradientLUTColorBar.ComponentTitle = ''

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(clip1)
# ----------------------------------------------------------------

# save screenshot
SaveScreenshot(sys.argv[2], renderView1, ImageResolution=[1102, 783],
    OverrideColorPalette='PrintBackground')
