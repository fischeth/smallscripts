#!/bin/python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
import math

df = pd.read_csv(sys.argv[1])

# compute calculation time
df['calculation_time'] = df['execution_max'] - df['io_max_ts0'] - df['io_max_ts25']
df['p_times_execution_time'] = df['#partitions'] * df['execution_max']
df['p_times_calculation_time'] = df['#partitions'] * df['calculation_time']

# group the data
grouped_df_min=df.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_mean=df.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_median=df.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_max=df.groupby(['#partitions'], as_index=False).aggregate(np.max)

def plotParallelEfficiency(grouped_df, column_name, l1):
    p0_t0=grouped_df[column_name][0]
    ax.plot(grouped_df['#partitions'], [p0_t0 / x for x in grouped_df[column_name]], marker='+', label=l1)

fig, ax = plt.subplots(figsize=(8, 5))

plotParallelEfficiency(grouped_df_min, 'p_times_execution_time', 'calculations + IO')
plotParallelEfficiency(grouped_df_min, 'p_times_calculation_time', 'calculations only')

#min_mpi_processes=df.iloc[0,0]
#p0_t0 = df.iloc[0,8] * min_mpi_processes
#ax.plot(df.iloc[:,0], [p0_t0 / x for x in df.iloc[:,8]*df.iloc[:,0]], marker='P', label='total simulation')

#p0_calculation_time = df.iloc[0,10] * min_mpi_processes
#df['p_times_calculation_time'] = df['#partitions'] * df['calculation_time']
#ax.plot(df.iloc[:,0], [p0_calculation_time / x for x in df['p_times_calculation_time']], marker='o', label='pure computations')

ax.set_title('OGS-6 Parallel Efficiency for the Hydro-thermal Process in Fractured Cube Example')
ax.legend()
ax.set_xlabel('number of MPI processes $[N]$')
min_mpi_processes=grouped_df_min['#partitions'][0]
ax.set_ylabel('parallel efficiency $\\left[\\frac{' + str(min_mpi_processes) + '\cdot t(' + str(min_mpi_processes) + ')}{N \cdot t(N)}\\right]$')

ax.grid(True, linestyle='-.')

fig.tight_layout()
fig.savefig('parallel_efficiency_juwels_rev3.png', transparent=True, dpi=300)

plt.show()

