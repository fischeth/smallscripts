#!/usr/bin/env python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
import math

df = pd.read_csv(sys.argv[1],sep=',',header='infer')

plot_scaling_min=False
plot_scaling_max=False
plot_scaling_median=False
plot_scaling_mean=False

plot_scaling_execution=False
plot_scaling_calculation=False

for arg in sys.argv:
    if (arg in ("--min")):
        plot_scaling_min=True
    if (arg in ("--max")):
        plot_scaling_max=True
    if (arg in ("--median")):
        plot_scaling_median=True
    if (arg in ("--mean")):
        plot_scaling_mean=True
    if (arg in ("--execution")):
        plot_scaling_execution=True
    if (arg in ("--calculation")):
        plot_scaling_calculation=True

fig, ax = plt.subplots(figsize=(8, 5))

last_index=len(df.iloc[:,0])-1
upper_idealy = df.iloc[last_index,0]/df.iloc[0,0]
idealy = np.linspace(1.0, upper_idealy)
idealx = [x * df.iloc[0,0] for x in idealy]
ax.plot(idealx, idealy, 'k--', label='ideal')

# compute calculation time
df['calculation_time'] = df['execution_max'] - df['io_max_ts0'] - df['io_max_ts25']

# group the data
grouped_df_min=df.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_mean=df.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_median=df.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_max=df.groupby(['#partitions'], as_index=False).aggregate(np.max)

def doPlot(grouped_df, df, column_name, l1, l2):
    s = grouped_df[column_name][0]
    ax.plot(grouped_df.iloc[:,0], [s / x for x in grouped_df[column_name]], marker='+', label=l1)
    #ax.plot(df.iloc[:,0], [s / x for x in df[column_name]], marker='x', label=l2, linestyle='')

# scaling based on mean
if (plot_scaling_mean):
    if (plot_scaling_execution):
        doPlot(grouped_df_mean, df, 'execution_max', 'calculations + IO (by mean)', 'total simulation')

    if (plot_scaling_calculation):
        doPlot(grouped_df_mean, df, 'calculation_time', 'calculations only (by mean)', 'calculation')

# scaling based on median
if (plot_scaling_median):
    if (plot_scaling_execution):
        doPlot(grouped_df_median, df, 'execution_max', 'calculations + IO (by median)', 'total simulation')

    if (plot_scaling_calculation):
        doPlot(grouped_df_median, df, 'calculation_time', 'calculations only (by median)', 'calculation')

# scaling based on min
if (plot_scaling_min):
    if (plot_scaling_execution):
        #doPlot(grouped_df_min, df, 'execution_max', 'calculations + IO (by min)', 'total simulation')
        doPlot(grouped_df_min, df, 'execution_max', 'scaling of calculation and IO', 'total simulation')

    if (plot_scaling_calculation):
        #doPlot(grouped_df_min, df, 'calculation_time', 'calculations only (by min)', 'calculation')
        doPlot(grouped_df_min, df, 'calculation_time', 'scaling of calculation only', 'calculation')

# scaling based on max
if (plot_scaling_max):
    if (plot_scaling_execution):
        doPlot(grouped_df_max, df, 'execution_max', 'calculations + IO (by max)', 'total simulation')

    if (plot_scaling_calculation):
        doPlot(grouped_df_max, df, 'calculation_time', 'calculations only (by max)', 'calculation')

ax.set_title('OGS-6 Strong Scaling for the Hydro-thermal Process in Fractured Cube Example')

ax.legend()
#ax.set_xlabel(df.columns.values[0])
ax.set_xlabel('number of MPI processes $[N]$')
ax.set_ylabel('scaling $\\left[\\frac{t(432)}{t(N)}\\right]$')
ax.grid(True, linestyle='-.')

fig.tight_layout()
fig.savefig('/tmp/scaling_ht_cube_juwels.png', transparent=True, dpi=300)
fig.savefig('/tmp/scaling_ht_cube_juwels.pdf', transparent=True, dpi=300)

plt.show()

