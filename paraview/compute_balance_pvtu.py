# trace generated using paraview version 5.7.0

#### import the simple module from the paraview
import sys
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML Partitioned Unstructured Grid Reader'
grid = XMLPartitionedUnstructuredGridReader(FileName=[sys.argv[1]])
grid.CellArrayStatus = ['MaterialIDs', 'bulk_element_ids', 'bulk_face_ids', 'specific_flux']
grid.PointArrayStatus = ['bulk_node_ids', 'initial_pressure', 'initial_temperature']

# create a new 'Integrate Variables'
integrateVariables1 = IntegrateVariables(Input=grid)

# Create a new 'SpreadSheet View'
spreadSheetView1 = CreateView('SpreadSheetView')
spreadSheetView1.ColumnToSort = ''
spreadSheetView1.BlockSize = 1024

# show data in view
integrateVariables1Display = Show(integrateVariables1, spreadSheetView1)

# get layout
layout1 = GetLayoutByName("Layout #1")

# add view to a layout so it's visible in UI
AssignViewToLayout(view=spreadSheetView1, layout=layout1, hint=0)

# Properties modified on spreadSheetView1
spreadSheetView1.FieldAssociation = 'Cell Data'

# Properties modified on spreadSheetView1
spreadSheetView1.HiddenColumnLabels = ['Cell ID', 'Area', 'Cell Type', 'MaterialIDs', 'bulk_element_ids', 'bulk_face_ids']

# export view
ExportView(sys.argv[2], view=spreadSheetView1)
