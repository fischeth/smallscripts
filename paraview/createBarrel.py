# trace generated using paraview version 5.6.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
import sys
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML Unstructured Grid Reader'
avtu = XMLUnstructuredGridReader(FileName=[sys.argv[1]])
avtu.CellArrayStatus = ['MaterialIDs']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1452, 794]

# show data in view
avtuDisplay = Show(avtu, renderView1)

# trace defaults for the display properties.
avtuDisplay.Representation = 'Surface'
avtuDisplay.ColorArrayName = [None, '']
avtuDisplay.OSPRayScaleArray = 'MaterialIDs'
avtuDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
avtuDisplay.SelectOrientationVectors = 'None'
avtuDisplay.ScaleFactor = 0.2
avtuDisplay.SelectScaleArray = 'None'
avtuDisplay.GlyphType = 'Arrow'
avtuDisplay.GlyphTableIndexArray = 'None'
avtuDisplay.GaussianRadius = 0.01
avtuDisplay.SetScaleArray = ['POINTS', 'MaterialIDs']
avtuDisplay.ScaleTransferFunction = 'PiecewiseFunction'
avtuDisplay.OpacityArray = ['POINTS', 'MaterialIDs']
avtuDisplay.OpacityTransferFunction = 'PiecewiseFunction'
avtuDisplay.DataAxesGrid = 'GridAxesRepresentation'
avtuDisplay.SelectionCellLabelFontFile = ''
avtuDisplay.SelectionPointLabelFontFile = ''
avtuDisplay.PolarAxes = 'PolarAxesRepresentation'
avtuDisplay.ScalarOpacityUnitDistance = 0.04294836305412413

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
avtuDisplay.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
avtuDisplay.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
avtuDisplay.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
avtuDisplay.DataAxesGrid.XTitleFontFile = ''
avtuDisplay.DataAxesGrid.YTitleFontFile = ''
avtuDisplay.DataAxesGrid.ZTitleFontFile = ''
avtuDisplay.DataAxesGrid.XLabelFontFile = ''
avtuDisplay.DataAxesGrid.YLabelFontFile = ''
avtuDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
avtuDisplay.PolarAxes.PolarAxisTitleFontFile = ''
avtuDisplay.PolarAxes.PolarAxisLabelFontFile = ''
avtuDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
avtuDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Extract Surface'
extractSurface1 = ExtractSurface(Input=avtu)

# show data in view
extractSurface1Display = Show(extractSurface1, renderView1)

# trace defaults for the display properties.
extractSurface1Display.Representation = 'Surface'
extractSurface1Display.ColorArrayName = [None, '']
extractSurface1Display.OSPRayScaleArray = 'MaterialIDs'
extractSurface1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractSurface1Display.SelectOrientationVectors = 'None'
extractSurface1Display.ScaleFactor = 0.2
extractSurface1Display.SelectScaleArray = 'None'
extractSurface1Display.GlyphType = 'Arrow'
extractSurface1Display.GlyphTableIndexArray = 'None'
extractSurface1Display.GaussianRadius = 0.01
extractSurface1Display.SetScaleArray = ['POINTS', 'MaterialIDs']
extractSurface1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractSurface1Display.OpacityArray = ['POINTS', 'MaterialIDs']
extractSurface1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractSurface1Display.DataAxesGrid = 'GridAxesRepresentation'
extractSurface1Display.SelectionCellLabelFontFile = ''
extractSurface1Display.SelectionPointLabelFontFile = ''
extractSurface1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
extractSurface1Display.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
extractSurface1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
extractSurface1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
extractSurface1Display.DataAxesGrid.XTitleFontFile = ''
extractSurface1Display.DataAxesGrid.YTitleFontFile = ''
extractSurface1Display.DataAxesGrid.ZTitleFontFile = ''
extractSurface1Display.DataAxesGrid.XLabelFontFile = ''
extractSurface1Display.DataAxesGrid.YLabelFontFile = ''
extractSurface1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
extractSurface1Display.PolarAxes.PolarAxisTitleFontFile = ''
extractSurface1Display.PolarAxes.PolarAxisLabelFontFile = ''
extractSurface1Display.PolarAxes.LastRadialAxisTextFontFile = ''
extractSurface1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(avtu, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# reset view to fit data
renderView1.ResetCamera()

# set active source
SetActiveSource(extractSurface1)

# reset view to fit data
renderView1.ResetCamera()

# reset view to fit data
renderView1.ResetCamera()

# reset view to fit data
renderView1.ResetCamera()

# create a new 'Extract Selection'
extractSelection1 = ExtractSelection(Input=extractSurface1,
    Selection=None)

# show data in view
extractSelection1Display = Show(extractSelection1, renderView1)

# trace defaults for the display properties.
extractSelection1Display.Representation = 'Surface'
extractSelection1Display.ColorArrayName = [None, '']
extractSelection1Display.OSPRayScaleArray = 'MaterialIDs'
extractSelection1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractSelection1Display.SelectOrientationVectors = 'None'
extractSelection1Display.ScaleFactor = 0.2
extractSelection1Display.SelectScaleArray = 'None'
extractSelection1Display.GlyphType = 'Arrow'
extractSelection1Display.GlyphTableIndexArray = 'None'
extractSelection1Display.GaussianRadius = 0.01
extractSelection1Display.SetScaleArray = ['POINTS', 'MaterialIDs']
extractSelection1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractSelection1Display.OpacityArray = ['POINTS', 'MaterialIDs']
extractSelection1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractSelection1Display.DataAxesGrid = 'GridAxesRepresentation'
extractSelection1Display.SelectionCellLabelFontFile = ''
extractSelection1Display.SelectionPointLabelFontFile = ''
extractSelection1Display.PolarAxes = 'PolarAxesRepresentation'
extractSelection1Display.ScalarOpacityUnitDistance = 0.1782910711921545

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
extractSelection1Display.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
extractSelection1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
extractSelection1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
extractSelection1Display.DataAxesGrid.XTitleFontFile = ''
extractSelection1Display.DataAxesGrid.YTitleFontFile = ''
extractSelection1Display.DataAxesGrid.ZTitleFontFile = ''
extractSelection1Display.DataAxesGrid.XLabelFontFile = ''
extractSelection1Display.DataAxesGrid.YLabelFontFile = ''
extractSelection1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
extractSelection1Display.PolarAxes.PolarAxisTitleFontFile = ''
extractSelection1Display.PolarAxes.PolarAxisLabelFontFile = ''
extractSelection1Display.PolarAxes.LastRadialAxisTextFontFile = ''
extractSelection1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(extractSurface1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# save data
SaveData(sys.argv[2], proxy=extractSelection1)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [0.0, 5.467515612208066, 0.05000000074505806]
renderView1.CameraFocalPoint = [0.0, 0.0, 0.05000000074505806]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1.4150971698348158

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
