#!/usr/bin/env bash

BIN=$HOME/w/o/cr/bin

$BIN/removeMeshElements -i $1_entire_boundary.vtu --x-min 0.0 --x-max 0.99 --y-min 0 --y-max 1 --z-min 0 --z-max 1 -o $1_right_boundary.vtu

## remove elements outside the middle area
# top elements
$BIN/removeMeshElements -i $1_right_boundary.vtu --x-min 0.0 --x-max 1 --y-min 0 --y-max 1 --z-min 0.68 --z-max 1 -o /tmp/b.vtu
# bottom elements
$BIN/removeMeshElements -i /tmp/b.vtu --x-min 0.0 --x-max 1 --y-min 0 --y-max 1 --z-min 0.0 --z-max 0.32 -o /tmp/c.vtu
# front
$BIN/removeMeshElements -i /tmp/c.vtu --x-min 0.0 --x-max 1 --y-min 0 --y-max 0.32 --z-min 0.0 --z-max 1 -o /tmp/d.vtu
# back
$BIN/removeMeshElements -i /tmp/d.vtu --x-min 0.0 --x-max 1 --y-min 0.68 --y-max 1 --z-min 0.0 --z-max 1 -o $1_part_of_right_boundary.vtu

## remove elements outside the middle area
# top elements
$BIN/removeMeshElements -i $1_right_boundary.vtu --x-min 0.0 --x-max 1 --y-min 0 --y-max 1 --z-min 0.96 --z-max 1 -o /tmp/b.vtu
# bottom elements
$BIN/removeMeshElements -i /tmp/b.vtu --x-min 0.0 --x-max 1 --y-min 0 --y-max 1 --z-min 0.0 --z-max 0.04 -o /tmp/c.vtu
# front
$BIN/removeMeshElements -i /tmp/c.vtu --x-min 0.0 --x-max 1 --y-min 0 --y-max 0.04 --z-min 0.0 --z-max 1 -o /tmp/d.vtu
# back
$BIN/removeMeshElements -i /tmp/d.vtu --x-min 0.0 --x-max 1 --y-min 0.96 --y-max 1 --z-min 0.0 --z-max 1 -o $1_larger_part_of_right_boundary.vtu

