#include <cmath>
#include <fstream>
#include <string>

void writeStationHeader(std::ostream& os)
{
    os << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
    os << "<?xml-stylesheet type=\"text/xsl\" href=\"OpenGeoSysSTN.xsl\"?>\n\n";
    os << "<!DOCTYPE OGS-STN-DOM>\n";
    os << "<OpenGeoSysSTN xmlns:xsi=";
    os << "\"http://www.w3.org/2001/XMLSchema-instance\" ";
    os << "xmlns:ogs=\"http://www.opengeosys.org\">\n";
}

void writeHeader(std::ostream& os)
{
    os << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
    os << "<?xml-stylesheet type=\"text/xsl\" href=\"OpenGeoSysGLI.xsl\"?>\n";
    os << "<OpenGeoSysGLI xmlns:xsi=";
    os << "\"http://www.w3.org/2001/XMLSchema-instance\" ";
    os << "xmlns:ogs=\"http://www.opengeosys.org\">\n";
}

int main(int argc, char* argv[])
{
    constexpr double const PI = std::atan(1.0) * 4;
    constexpr double const r = 0.95;
    int n_samples = 200;
    const double x0 = 0.0;
    const double y0 = 0.0;
    std::ofstream out("circle_r_" + std::to_string(r) + "_points_" + std::to_string(n_samples) + ".gml");
    writeHeader(out);
    out << "  <name>Circles</name>" << std::endl;
    out << "  <points>" << std::endl;
    for (int n = 0; n < n_samples; ++n)
    {
        out << "    <point x=\"" << x0 + r * cos(2 * PI * (n_samples - n) / n_samples)
            << "\" y=\"" << y0 + r * sin(2 * PI * (n_samples - n) / n_samples)
            << "\" z=\"0.0\" id=\"" << n << "\"/>" << std::endl;
    }
    constexpr int const s = 20;
    constexpr double const r_in = r / s;
    for (int n = 0; n < n_samples/s; ++n)
    {
        out << "    <point x=\"" << x0 + r/s * cos(2 * PI * (n_samples - s*n) / n_samples)
            << "\" y=\"" << y0 + r/s * sin(2 * PI * (n_samples - s*n) / n_samples)
            << "\" z=\"0.0\" id=\"" << n_samples + n << "\"/>" << std::endl;
    }
    out << "  </points>" << std::endl;

    out << "  <polylines>" << std::endl;
    out << "    <polyline id=\"0\" name=\"outer_circle\">" << std::endl;
    for (int n = 0; n < n_samples; ++n)
    {
        out << "      <pnt>" << n << "</pnt>" << std::endl;
    }
    out << "      <pnt>0</pnt>" << std::endl;
    out << "    </polyline>" << std::endl;
    out << "    <polyline id=\"1\" name=\"inner_circle\">" << std::endl;
    for (int n = n_samples; n < n_samples + n_samples/s; ++n)
    {
        out << "      <pnt>" << n << "</pnt>" << std::endl;
    }
    out << "      <pnt>" << n_samples << "</pnt>" << std::endl;
    out << "    </polyline>" << std::endl;
    out << "  </polylines>" << std::endl;
    out << "</OpenGeoSysGLI>";
    out.close();

    // write midpoint as station
    std::ofstream middle_point_out("circle_r_" + std::to_string(r) +
                                   "_points_" + std::to_string(n_samples) +
                                   ".stn");

    writeStationHeader(middle_point_out);
    middle_point_out << "<stationlist>\n";
    middle_point_out << "  <name>circle_midpoint</name>\n";
    middle_point_out << "  <stations>\n";
    middle_point_out << "    <station x=\"" << x0 << "\" y=\"" << y0
                     << "\" z=\"0.0\" id=\"0\">" << std::endl;
    middle_point_out << "      <name>midpoint</name>\n";
    middle_point_out << "      <value>0</value>\n";
    middle_point_out << "    </station>\n";
    middle_point_out << "  </stations>\n";
    middle_point_out << "</stationlist>\n";
    middle_point_out << "</OpenGeoSysSTN>";
    middle_point_out.close();
    return 0;
}
