#include <cmath>
#include <fstream>
#include <string>

void writeStationHeader(std::ostream& os)
{
    os << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
    os << "<?xml-stylesheet type=\"text/xsl\" href=\"OpenGeoSysSTN.xsl\"?>\n\n";
    os << "<!DOCTYPE OGS-STN-DOM>\n";
    os << "<OpenGeoSysSTN xmlns:xsi=";
    os << "\"http://www.w3.org/2001/XMLSchema-instance\" ";
    os << "xmlns:ogs=\"http://www.opengeosys.org\">\n";
}

void writeHeader(std::ostream& os)
{
    os << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
    os << "<?xml-stylesheet type=\"text/xsl\" href=\"OpenGeoSysGLI.xsl\"?>\n";
    os << "<OpenGeoSysGLI xmlns:xsi=";
    os << "\"http://www.w3.org/2001/XMLSchema-instance\" ";
    os << "xmlns:ogs=\"http://www.opengeosys.org\">\n";
}

int main(int argc, char* argv[])
{
    const double PI = std::atan(1.0) * 4;
    const double r = 1;
    int n_samples = 50;
    const double x0 = 0.0;
    const double y0 = 0.0;
    std::ofstream out("hemicircle_r_" + std::to_string(r) + "_points_" + std::to_string(n_samples) + ".gml");
    writeHeader(out);
    out << "  <name>HemiCircle</name>" << std::endl;
    out << "  <points>" << std::endl;
    for (int n = 0; n <= n_samples; ++n)
    {
        out << "    <point x=\"" << x0 + r * cos(PI * (n_samples - n) / n_samples)
            << "\" y=\"" << y0 + r * sin(PI * (n_samples - n) / n_samples)
            << "\" z=\"0.0\" id=\"" << n << "\"/>" << std::endl;
    }
    /*
    out << "    <point x=\"" << x0 + r / 100 << "\" y=\"" << y0
        << "\" z=\"0.0\" id=\"" << n_samples + 1 << "\"/>" << std::endl;
    out << "    <point x=\"" << x0 << "\" y=\"" << y0
        << "\" z=\"0.0\" id=\"" << n_samples + 2 << "\"/>" << std::endl;
    out << "    <point x=\"" << x0 - r / 100 << "\" y=\"" << y0
        << "\" z=\"0.0\" id=\"" << n_samples + 3 << "\"/>" << std::endl;
    */
    out << "  </points>" << std::endl;
    out << "  <polylines>" << std::endl;
    out << "    <polyline id=\"0\" name=\"circle\">" << std::endl;
    for (int n = 0; n <= n_samples; ++n)
    {
        out << "      <pnt>" << n << "</pnt>" << std::endl;
    }
    out << "      <pnt>0</pnt>" << std::endl;
    out << "    </polyline>" << std::endl;
    out << "  </polylines>" << std::endl;
    out << "</OpenGeoSysGLI>";
    out.close();

    // write midpoint as station
    std::ofstream middle_point_out("circle_r_" + std::to_string(r) +
                                   "_points_" + std::to_string(n_samples) +
                                   ".stn");

    writeStationHeader(middle_point_out);
    middle_point_out << "<stationlist>\n";
    middle_point_out << "  <name>circle_midpoint</name>\n";
    middle_point_out << "  <stations>\n";
    middle_point_out << "    <station x=\"" << x0-r/100 << "\" y=\"" << y0
                     << "\" z=\"0.0\" id=\"0\">" << std::endl;
    middle_point_out << "      <name>left_from_midpoint</name>\n";
    middle_point_out << "      <value>0</value>\n";
    middle_point_out << "    </station>\n";
    middle_point_out << "    <station x=\"" << x0 << "\" y=\"" << y0
                     << "\" z=\"0.0\" id=\"1\">" << std::endl;
    middle_point_out << "      <name>midpoint</name>\n";
    middle_point_out << "      <value>0</value>\n";
    middle_point_out << "    </station>\n";
    middle_point_out << "    <station x=\"" << x0+r/100 << "\" y=\"" << y0
                     << "\" z=\"0.0\" id=\"2\">" << std::endl;
    middle_point_out << "      <name>right_from_midpoint</name>\n";
    middle_point_out << "      <value>0</value>\n";
    middle_point_out << "    </station>\n";
    middle_point_out << "  </stations>\n";
    middle_point_out << "</stationlist>\n";
    middle_point_out << "</OpenGeoSysSTN>";
    middle_point_out.close();
    return 0;
}
