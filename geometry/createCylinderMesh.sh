#!/bin/bash

OGS_BINARY_PATH=$HOME/w/o/cr/bin

layer_thickness=0.1

# convert gmsh mesh to vtu
$OGS_BINARY_PATH/GMSH2OGS -i $1 -o /tmp/b.vtu

for i in `seq 1 10`;
do
    $OGS_BINARY_PATH/AddTopLayer -i /tmp/b.vtu -o /tmp/c.vtu -t $layer_thickness
    mv /tmp/c.vtu /tmp/b.vtu
done

# remove triangles, lines, and quads
$OGS_BINARY_PATH/removeMeshElements -t line -i /tmp/b.vtu -o /tmp/c.vtu
$OGS_BINARY_PATH/removeMeshElements -t tri -i /tmp/c.vtu -o /tmp/b.vtu
$OGS_BINARY_PATH/removeMeshElements -t quad -i /tmp/b.vtu -o /tmp/c.vtu

mv /tmp/c.vtu /tmp/a.vtu
rm /tmp/b.vtu
