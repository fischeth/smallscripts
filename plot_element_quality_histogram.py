#!/bin/python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd

#plt.style.use('ggplot')
#plt.style.use('dark_background')
plt.style.use('Solarize_Light2')

df = pd.read_csv(sys.argv[1])
#df1 = pd.read_csv(sys.argv[2])

fig = plt.figure(figsize=(8, 6))
plt.title('Element Quality')

ax1 = fig.add_subplot(111)

ax1.set_title('Slice 2')
#bins1 = [1e-30, 1e-10, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 0.1, 1]
#ax1.hist(df.iloc[:,0], bins1)
ax1.hist(df.iloc[:,0])
ax1.grid() #True, linestyle='-.')
#ax1.set_yscale('log')
#ax1.set_xscale('log')
ax1.set_xlabel('aspect ratio')
ax1.set_ylabel('number of elements')

#ax2 = fig.add_subplot(122)
##bins2 = [1e-30, 1e-10, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 0.1, 1]
#ax2.hist(df.iloc[:,0])
#ax2.set_title('Slice 2')
#ax2.grid() #True, linestyle='-.')
##ax2.set_xscale('log')
##ax2.set_yscale('log')
##ax2.set_xlabel('aspect ratio')
##ax2.set_ylabel('number of elements')

print(plt.style.available)

fig.savefig('histogram.pdf', transparent=True)
plt.show()

