#!/bin/python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd

df = pd.read_csv(sys.argv[1], sep=' ')

fig, ax = plt.subplots(figsize=(8, 5))

ax.plot(df.iloc[:,0], df.iloc[:,1], marker='o')

#ax.legend()
if (len(sys.argv) == 3):
    ax.set_title(sys.argv[2])
ax.set_xlabel(df.columns.values[0])
ax.set_ylabel(df.columns.values[1])
ax.grid() #True, linestyle='-.')
#ax.set_yscale('log')
ax.set_xscale('log')
plt.show()

